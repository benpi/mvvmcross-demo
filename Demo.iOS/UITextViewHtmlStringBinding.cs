﻿using System;
using Foundation;
using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target;
using UIKit;

namespace Demo.iOS
{
	public class UITextViewHtmlStringBinding : MvxTargetBinding
	{
		// the binding gets set in code so we store the binding key here
		// to make usage keyword refactor-able
		public static readonly string Key = "HtmlText";

		public override MvxBindingMode DefaultMode {
			get {
				return MvxBindingMode.OneWay;
			}
		}

		public override Type TargetType {
			get {
				return typeof (string);
			}
		}

		public UITextViewHtmlStringBinding (UITextView target) : base (target)
		{

		}

		public override void SetValue (object value)
		{
			var htmlString = value as string;
			if (htmlString == null) {
				((UITextView)Target).Text = string.Empty;
			} else {
				NSError error = null;

				var attributes = new NSAttributedStringDocumentAttributes {
					DocumentType = NSDocumentType.HTML
				};

				var attributedHtml = new NSAttributedString (
										htmlString,
										attributes,
									 	ref error);

				// todo: error handling

				((UITextView)Target).AttributedText = attributedHtml;
			}
		}
	}
}
