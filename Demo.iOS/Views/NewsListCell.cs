﻿using System;
using Demo.Core.Data.Models;
using Demo.iOS.Converter;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace Demo.iOS.Views
{
	public partial class NewsListCell : MvxTableViewCell
	{
		public static readonly NSString Key = new NSString ("NewsListCell");
		public static readonly UINib Nib;

		static NewsListCell ()
		{
			Nib = UINib.FromName ("NewsListCell", NSBundle.MainBundle);
		}

		protected NewsListCell (IntPtr handle) : base (handle)
		{
			var imageViewLoader = new MvxImageViewLoader (() => image);

			this.DelayBind (() => {
				var set = this.CreateBindingSet<NewsListCell, RssItem> ();

				set.Bind (imageViewLoader)
				   .To (rssItem => rssItem.ThumbnailUrl);
				set.Bind (titleLabel)
				   .To (rssItem => rssItem.Title);
				set.Bind (descriptionLabel)
				   .To (rssItem => rssItem.Description)
				   .WithConversion(new RemoveHtmlTagsConverter());
				
				set.Apply ();
			});
		}
	}
}
