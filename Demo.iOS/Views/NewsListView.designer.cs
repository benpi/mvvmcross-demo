// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Demo.iOS.Views
{
    [Register ("NewsListView")]
    partial class NewsListView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView newsTableView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton refreshButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (newsTableView != null) {
                newsTableView.Dispose ();
                newsTableView = null;
            }

            if (refreshButton != null) {
                refreshButton.Dispose ();
                refreshButton = null;
            }
        }
    }
}