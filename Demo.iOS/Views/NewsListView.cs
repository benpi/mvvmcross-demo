﻿using System;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Binding.BindingContext;
using UIKit;
using Demo.Core.ViewModels;
using MvvmCross.iOS.Views;

namespace Demo.iOS.Views
{
	public partial class NewsListView : MvxViewController
	{
		public NewsListView () : base (nameof(NewsListView), null)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			var source = new MvxSimpleTableViewSource (newsTableView, nameof(NewsListCell), NewsListCell.Key);
			newsTableView.RowHeight = 100;

			var set = this.CreateBindingSet<NewsListView, NewsListViewModel> ();

			set.Bind (source)
			   .To (newsListViewModel => newsListViewModel.RssItems);
			set.Bind (source)
			   .For (s => s.SelectionChangedCommand)
			   .To (newsListViewModel => newsListViewModel.OpenDetailsCommand);
			set.Bind (refreshButton)
			   .To (newsListViewModel => newsListViewModel.RefreshCommand);
			
			set.Apply ();

			newsTableView.Source = source;
			newsTableView.ReloadData ();
		}
	}
}

