﻿using Demo.Core.ViewModels;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.iOS.Views;

namespace Demo.iOS.Views
{
	public partial class NewsDetailView : MvxViewController
	{
		public NewsDetailView () : base (nameof (NewsDetailView), null)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			linkTextView.TextContainer.MaximumNumberOfLines = 1;
			linkTextView.TextContainer.LineBreakMode = UIKit.UILineBreakMode.TailTruncation;

			var imageViewLoader = new MvxImageViewLoader (() => newsImage);

			var set = this.CreateBindingSet<NewsDetailView, NewsDetailViewModel> ();

			set.Bind (imageViewLoader)
			   .To (newsDetailViewModel => newsDetailViewModel.RssItem.ThumbnailUrl);
			set.Bind (titleLabel)
			   .To (newsDetailViewModel => newsDetailViewModel.RssItem.Title);
			set.Bind (contentTextView)
			   .For(UITextViewHtmlStringBinding.Key)
			   .To (newsDetailViewModel => newsDetailViewModel.RssItem.Content);
			set.Bind (linkTextView)
			   .To (newsDetailViewModel => newsDetailViewModel.RssItem.Link);

			set.Apply ();
		}
	}
}

