﻿using Demo.Core.Data.Models;
using Demo.Core.Data.Services;
using MvvmCross.Core.ViewModels;

namespace Demo.Core.ViewModels
{
    public class NewsDetailViewModel : MvxViewModel
    {
        private readonly IDataService _dataService;

        private RssItem _rssItem;

        public RssItem RssItem
        {
            get { return _rssItem; }
            set
            {
                _rssItem = value;
                RaisePropertyChanged(() => RssItem);
            }
        }

        public NewsDetailViewModel(IDataService dataService)
        {
            _dataService = dataService;
        }
        
        // "Magic" init method that gets resolved by mvvmcross automatically. 
        // The mapping happens because in the navigation we create an anonymous object with a string field named "title".
        // This field gets mapped to the "title" parameter of this method.
        public async void Init(string title)
        {
            RssItem = await _dataService.GetItemAsync(title);
        }
    }
}
