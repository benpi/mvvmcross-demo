﻿using System;
using System.Collections.ObjectModel;
using Demo.Core.Data.Models;
using Demo.Core.Data.Services;
using MvvmCross.Core.ViewModels;

namespace Demo.Core.ViewModels
{
    public class NewsListViewModel : MvxViewModel
    {
        private readonly IDataService _dataService;

        private bool _isRefreshing;

        public ObservableCollection<RssItem> RssItems { get; }

        public MvxCommand RefreshCommand { get; }
        public MvxCommand<RssItem> OpenDetailsCommand { get; }

        public NewsListViewModel(IDataService dataService)
        {
            _dataService = dataService;

            RssItems = new ObservableCollection<RssItem>();

            RefreshCommand = new MvxCommand(
                () => RefreshRssItems(), () => !_isRefreshing);
            OpenDetailsCommand = new MvxCommand<RssItem>(OpenDetails);
        }

        public override void Start()
        {
            base.Start();
            RefreshRssItems(false);
        }

        private async void RefreshRssItems(bool refresh = true)
        {
            try // todo: errorhandling
            {
                _isRefreshing = true;
                RefreshCommand.RaiseCanExecuteChanged();

                var newItems = await _dataService.GetFeedAsync(refresh);

                RssItems.Clear();
                foreach (var newItem in newItems)
                {
                    RssItems.Add(newItem);
                }
            }
            finally
            {
                _isRefreshing = false;
                RefreshCommand.RaiseCanExecuteChanged();
            }
        }

        private void OpenDetails(RssItem item)
        {
            if (item != null)
            {
                ShowViewModel<NewsDetailViewModel>(new {title = item.Title});
            }
        }
    }
}