﻿using System.Collections.Generic;
using System.Xml.Linq;
using Demo.Core.Data.Models;

namespace Demo.Core.Helper
{
    public static class FeedParser
    {
        public static RssItem[] ParseXmlContent(string contentString)
        {
            var doc = XDocument.Parse(contentString);

            var rss = doc.Element("rss");
            var channel = rss?.Element("channel");
            var items = channel?.Elements("item");

            var rssItmList = new List<RssItem>();
            foreach (var xElement in items)
            {
                var rssItem = new RssItem();
                rssItem.Title = xElement.Element("title")?.Value;
                rssItem.Description = xElement.Element("description")?.Value;
                rssItem.Link = xElement.Element("link")?.Value;
                rssItem.Content = xElement.Element("{http://purl.org/rss/1.0/modules/content/}encoded")?.Value;
                var enclosure = xElement.Element("enclosure");
                if (enclosure != null)
                {
                    rssItem.ThumbnailUrl = enclosure.Attribute("url")?.Value;
                }

                rssItmList.Add(rssItem);
            }

            return rssItmList.ToArray();
        }
    }
}
