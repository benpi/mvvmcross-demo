using MvvmCross.Core.ViewModels;

namespace Demo.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            RegisterAppStart<ViewModels.NewsListViewModel>();
        }
    }
}

