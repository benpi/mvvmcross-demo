﻿namespace Demo.Core.Data.Models
{
    /// <summary>
    /// Represents an RSS feed item
    /// </summary>
    public class RssItem
    {
        public string Title { get; set; }
        public string Link { get; set; }
        public string ThumbnailUrl { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
    }
}
