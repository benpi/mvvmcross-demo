﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Linq;
using Demo.Core.Data.Models;
using Demo.Core.Helper;

namespace Demo.Core.Data.Services
{
    public class DataService : IDataService
    {
        private readonly HttpClient _httpClient;

        private RssItem[] _cache;

        // rss feed of entwickler.net
        private const string FeedUri = "https://entwickler.de/feed/rss2";

        public DataService()
        {
            _httpClient = new HttpClient();
        }

        public async Task<RssItem[]> GetFeedAsync(bool refresh)
        {
            if (!refresh && _cache != null)
            {
                return _cache;
            }

            var response = await _httpClient.GetAsync(FeedUri);
            var contentString = await response.Content.ReadAsStringAsync();

            var rssItems = FeedParser.ParseXmlContent(contentString);

            if (rssItems != null)
            {
                _cache = rssItems;
            }

            return rssItems;
        }

        public Task<RssItem> GetItemAsync(string title)
        {
            var rssItem = _cache?.FirstOrDefault(i => i.Title == title);
            return Task.FromResult(rssItem);
        }
    }
}