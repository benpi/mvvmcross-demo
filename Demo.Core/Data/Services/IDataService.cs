﻿using System.Threading.Tasks;
using Demo.Core.Data.Models;

namespace Demo.Core.Data.Services
{
    public interface IDataService
    {
        /// <summary>
        /// Gets current feed of RssItems
        /// </summary>
        /// <param name="refresh">If true, ignore cache and refresh data</param>
        /// <returns></returns>
        Task<RssItem[]> GetFeedAsync(bool refresh = false);

        /// <summary>
        /// Gets rss item by title
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        Task<RssItem> GetItemAsync(string title);
    }
}
