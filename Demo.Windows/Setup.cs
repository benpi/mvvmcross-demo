using Windows.UI.Xaml.Controls;
using Demo.Core.Data.Services;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.WindowsUWP.Platform;

namespace Demo.Windows
{
    public class Setup : MvxWindowsSetup
    {
        public Setup(Frame rootFrame) : base(rootFrame)
        {
        }

        protected override void InitializePlatformServices()
        {
            base.InitializePlatformServices();

            Mvx.LazyConstructAndRegisterSingleton<IDataService, DataService>();
        }

        protected override IMvxApplication CreateApp()
        {
            return new Core.App();
        }

    }
}