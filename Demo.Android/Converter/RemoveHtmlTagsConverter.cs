﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using MvvmCross.Platform.Converters;

namespace Demo.Droid.Converter
{
    public class RemoveHtmlTagsConverter : MvxValueConverter<string, string>
    {
        protected override string Convert(string value, Type targetType, object parameter, CultureInfo culture)
        {
            return ScrubHtml(value);
        }

        // from https://stackoverflow.com/questions/19523913/remove-html-tags-from-string-including-nbsp-in-c-sharp
        private static string ScrubHtml(string value)
        {
            var step1 = Regex.Replace(value, @"<[^>]+>|&nbsp;", "").Trim();
            var step2 = Regex.Replace(step1, @"\s{2,}", " ");
            return step2;
        }
    }
}