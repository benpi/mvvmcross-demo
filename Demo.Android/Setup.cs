using Android.Content;
using Android.Widget;
using Demo.Core.Data.Services;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Droid.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Platform.Platform;

namespace Demo.Droid
{
    public class Setup : MvxAndroidSetup
    {
        public Setup(Context applicationContext) : base(applicationContext)
        {
        }

        protected override IMvxApplication CreateApp()
        {
            return new Core.App();
        }

        protected override void InitializePlatformServices()
        {
            base.InitializePlatformServices();

            Mvx.LazyConstructAndRegisterSingleton<IDataService, DataService>();
        }

        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }

        protected override void FillTargetFactories(IMvxTargetBindingFactoryRegistry registry)
        {
            base.FillTargetFactories(registry);
            
            // here we can register custom bindings

            // custom binding for weblinks to images for imageView. Binding keyword is "ImageUrl" in the xml
            registry.RegisterCustomBindingFactory<ImageView>(
                "ImageUrl",
                imageView => new ImageViewUrlBinding(imageView));

            // custom bindings to load html formatted text into a textview
            registry.RegisterCustomBindingFactory<TextView>(
                "HtmlText",
                textView => new TextViewHtmlTextBinding(textView));
        }
    }
}
