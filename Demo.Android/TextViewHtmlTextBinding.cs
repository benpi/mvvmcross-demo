﻿using System;
using Android.OS;
using Android.Text;
using Android.Text.Method;
using Android.Widget;
using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target;

namespace Demo.Droid
{
    /// <summary>
    /// Targetbinding to make ImageView able to use string with html tags as content
    /// </summary>
    public class TextViewHtmlTextBinding : MvxTargetBinding
    {
        public override Type TargetType => typeof(string);

        public override MvxBindingMode DefaultMode => MvxBindingMode.OneWay;

        public TextViewHtmlTextBinding(TextView target) : base(target)
        {
        }

        public override void SetValue(object value)
        {
            if (Target is TextView tw)
            {
                var htmlText = Build.VERSION.SdkInt >= BuildVersionCodes.N ? 
                    Html.FromHtml(value as string, FromHtmlOptions.ModeLegacy) : 
                    Html.FromHtml(value as string);


                tw.TextFormatted = htmlText;
                tw.MovementMethod = LinkMovementMethod.Instance; // makes links clickable
            }
        }
    }
}