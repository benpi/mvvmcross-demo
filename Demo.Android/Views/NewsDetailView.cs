﻿using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;

namespace Demo.Droid.Views
{
    [Activity(Label = "NewsDetailView")]
    public class NewsDetailView : MvxActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.NewsDetailView);
        }
    }
}