﻿using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;

namespace Demo.Droid.Views
{
    [Activity(Label = "NewsListView")]
    public class NewsListView : MvxActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.NewsListView);
        }
    }
}