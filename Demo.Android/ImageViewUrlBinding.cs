﻿using System;
using Android.Graphics;
using Android.Util;
using Android.Widget;
using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target;
using MvvmCross.Platform;
using MvvmCross.Plugins.DownloadCache;

namespace Demo.Droid
{
    /// <summary>
    /// Custom binding for ImageView which attempts to load a bitmap from a url string.
    /// </summary>
    public class ImageViewUrlBinding : MvxTargetBinding
    {
        private readonly MvxImageCache<Bitmap> _imageCache = new MvxImageCache<Bitmap>(
            Mvx.Resolve<IMvxFileDownloadCache>(), 
            30,
            31457280, // 30 MByte
            true);

        // Defines data type that gets bound by binding
        public override Type TargetType => typeof(string);

        // Defines binding type
        public override MvxBindingMode DefaultMode => MvxBindingMode.OneWay;

        public ImageViewUrlBinding(ImageView target) : base(target)
        {
        }

        /// <summary>
        /// Gets triggered when the bound value changes
        /// </summary>
        /// <param name="value"></param>
        public override async void SetValue(object value)
        {
            if (Target is ImageView iv)
            {
                if (value is string url && Uri.IsWellFormedUriString(url, UriKind.RelativeOrAbsolute))
                {
                    // to not block ui thread, we download the bitmap in the background
                    // and set it in the imageview after the download finished

                    // set the imageview to transparent first
                    iv.SetImageResource(Android.Resource.Color.Transparent);
                    
                    // asynchronous retrieve bitmap from url or cache if downloaded before
                    var bitmap = await _imageCache.RequestImage(url);

                    // set retrieved image ti imageview
                    iv.SetImageBitmap(bitmap);
                }
                else
                {
                    Log.Debug(nameof(ImageViewUrlBinding), $"Url-string \"{value}\" was malformed!");
                    iv.SetImageResource(Android.Resource.Color.Transparent);
                }
            }
        }

    }
}